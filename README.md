# red-x

red-x is a browser extension or userscript for Reddit created with the intent to use Reddit without nonfree javascript. red-x currently does not block any JS itself, so it should be paired with LibreJS, uMatrix, Noscript or a similar add-on. However, blocking reddit's JS is a planned feature.

red-x currently only works with old reddit. That means you will need to either navigate to old.reddit.com or be have the redesign disabled in your account. If there is demand, I may work on new reddit. red-x also currently requires reddit cookies to be enabled.

If you need help with getting red-x setup, please by all means contact me!

## Installation

The latest release will be the very top tag in the [tags section](https://gitlab.com/xianbaum/red-x/tags). Please also check out the [Firefox add-on page](https://addons.mozilla.org/en-US/firefox/addon/red-x/), too.

The .xpi (webextension) version is compatible with Firefox-based web browsers that support webextensions. The add-ons page will prompt you to install it when you click "Add to Firefox".

If you can't use the webextension version, there is also a userscript version. Many browsers support userscripts in some way. Your userscript manager should prompt you to install it once you click on the userscript file. If it doesn't, you can import it manually.

## Continuous/Latest builds

I am hosting the very latest builds on my server. I will attempt to keep the server up 24/7 but it may go down from time to time.

The latest build userscript can be found [here](http://latenight.christianbaum.com:8000/red-x-latest/red-x.user.js). It should prompt to install automatically. Note that this does not currently have an auto-update feature.

The latest build webextension can be found [here](http://latenight.christianbaum.com:8000/red-x-latest/red-x.zip). To use this with Firefox-based browsers, you will need to enable unsigned addons through your about:config file and you will probably have to rename the .zip extension to .xpi.

## Features

red-x allows you to do the things that are essential to using reddit

* post things
* reply to things
* upvote and downvote things
* do general site things

## Feature requests

There is a lot of work to do on red-x. I don't know what features people want the most so if there is a specific features you want in red-x, please make an issue and I will make it a priority. Also consider forking it and then making a merge request!

## Bugs

There are a lot of problems with red-x. A lot of things just aren't implemented, so if you click on a certain button, it's possible that nothing may happen. I am working on implementing as much as I can, but if there is something that is bugging you, please make a bug report and I will make it a priority.

## Building

Pre-requisites:

node, npm, typescript and browserify

Web extension

>npm install

>npm run webextension

User script

>npm install

>npm run userscript

If it is a release build, run either webextension-release or userscript-release.

## License

MIT Licensed. See the LICENSE file.