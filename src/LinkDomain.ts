export enum LinkDomain {
    Subreddit,
    NpReddit,
    IReddit,
    VReddit,
    Imgur,
    Twitch,
    GfyCat,
    YouTube,
    Unknown
}

export function DomainStringToDomain(domain: string | undefined): LinkDomain {
    if(domain == undefined) {
	return LinkDomain.Unknown;
    }
    if(domain.indexOf("self.") == 0) {
        return LinkDomain.Subreddit; 
    }
    if(domain.indexOf("imgur.com") >= 0) {
        return LinkDomain.Imgur;
    }
    if(domain.indexOf("np.reddit.com") >= 0) {
        return LinkDomain.NpReddit;
    }
    if(domain.indexOf("i.reddit.com") >= 0 || domain.indexOf("i.redd.it") >= 0) {
        return LinkDomain.IReddit;
    }
    if(domain.indexOf("v.reddit.com") >= 0 || domain.indexOf("v.redd.it") >= 0){ 
        return LinkDomain.VReddit;
    }
    if(domain.indexOf("twitch.tv") >= 0) {
        return LinkDomain.Twitch;
    }
    if(domain.indexOf("gfycat.com") >= 0) {
        return LinkDomain.GfyCat;
    }
    if(domain.indexOf("youtube.com") >= 0 || domain.indexOf("youtu.be") >= 0) { 
        return LinkDomain.YouTube;
    }
    return LinkDomain.Unknown;
}
