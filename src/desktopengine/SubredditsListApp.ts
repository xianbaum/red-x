import { RedditElements } from "./RedditElements";

export class SubredditsListApp {
    static init() {
	let buttons = document.getElementsByClassName("subscribe-button");
	for(let button of buttons) {
	    RedditElements.hookSubscribeButton(
		button as HTMLSpanElement,
		button.getAttribute("data-sr_name")!);
	}
    }
}
