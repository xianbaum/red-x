import { Expando } from "../interfaces/Expando";
import { LinkDomain } from "../LinkDomain";
import { NotImplementedException } from "../helpers/NotImplementedException";
import { LinkCommentApi } from "../RedditApi";
import { RedditThread } from "../interfaces/RedditThread";

export function GetExpandoMethod(redditThread: RedditThread): () =>
    Promise<string | HTMLElement> {
    switch (redditThread.domain) {
        case LinkDomain.Subreddit:
            return redditExpandoGetter(redditThread);
        case LinkDomain.Imgur:
            return imgurExpandoGetter(redditThread);
        case LinkDomain.IReddit:
        case LinkDomain.VReddit:
            return () => {
                return Promise.reject(
		    "i.reddit.com and v.reddit.com are not (yet) supported");
            };
        case LinkDomain.YouTube:
            return () => {
                return Promise.reject("youtube-dl "+redditThread.link);
            }
        case LinkDomain.Twitch:
            return () => {
                return Promise.reject("streamlink "+redditThread.link);
            }
        default:
            return () => {
                return Promise.reject("This domain is not known.");
            };
    }
}

function redditExpandoGetter(redditThread: RedditThread) {
    let cachedSelftext: string
    return () => {
        if(cachedSelftext != undefined ) {
            return Promise.resolve(cachedSelftext)
        }
        return LinkCommentApi.getExpando(redditThread.fullname).then(
	    (response) => {
		cachedSelftext = response;
		return response;
            })
    }
}
function imgurExpandoGetter(redditThread: RedditThread) {
    let cachedvalue: HTMLElement | undefined;
    return () => {
        if(cachedvalue != undefined) {
            return Promise.resolve(cachedvalue);
        }
        if (redditThread.link == undefined) {
            return Promise.reject("Could not get link");
        }
        let linkToGet = redditThread.link;
        let isGifv: boolean = linkToGet.indexOf(".gifv") > -1;
        if (isGifv) {
            linkToGet = linkToGet.substr(0, linkToGet.length - 4);
        } else if (linkToGet.indexOf("/a/") >= 0) {
            return Promise.reject("Imgur albums are not supported (yet)");
        } else if (linkToGet.indexOf(".gif") == -1 &&
		   linkToGet.indexOf(".jpg") == -1) {
            let id = linkToGet.substring(
		linkToGet.indexOf(".com/") + 5,
		linkToGet.indexOf(".com/") + 12)
            linkToGet = "https://i.imgur.com/" + id + ".jpg";
        }
        return new Promise<HTMLElement>((resolve, reject) => {
            if (isGifv) {
                /* This fails 100% of the time because imgur wants you to use
		   their proprietary player. I wonder if I can figure this
		   out one day */
                let video = document.createElement("video");
                video.autoplay = true;
                video.src = linkToGet + "mp4";
                video.addEventListener("error", () => {
                    let img = document.createElement("img");
                    img.src = linkToGet + "gif";
                    cachedvalue = img;
                    resolve(img);
                });
                video.addEventListener("play", () => {
                    cachedvalue = video;
                    resolve(video);
                })
            } else {
                let img = document.createElement("img");
                img.src = linkToGet;
                cachedvalue = img;
                resolve(img);
            }
        });
    }
}
