import { RedditElements } from "./RedditElements";
import { RedditThread } from "../interfaces/RedditThread";
import { Dictionary } from "../helpers/Dictionary";
import { DesktopRedditThreadFromElement } from "./DesktopRedditThreadFromElement";
import { DesktopRedditComment } from "../interfaces/DesktopRedditComment";
import { DesktopRedditCommentFromElement } from "./DesktopRedditCommentFromElement";

export class SavedServices {
    constructor(){
        this.threads = 
            RedditElements.generateListOf<DesktopRedditThreadFromElement>(
		"link",
                DesktopRedditThreadFromElement);
        this.comments = 
            RedditElements.generateListOf<DesktopRedditCommentFromElement>(
		"comment",
                DesktopRedditCommentFromElement);
    }
    private threads: Dictionary<RedditThread>;
    private comments: Dictionary<DesktopRedditComment>;
}
