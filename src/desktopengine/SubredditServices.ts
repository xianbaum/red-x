import { RedditElements } from "./RedditElements";
import { RedditThread } from "../interfaces/RedditThread";
import { Dictionary } from "../helpers/Dictionary";
import { DesktopRedditThreadFromElement } from "./DesktopRedditThreadFromElement";
import { DesktopEngine } from "./DesktopEngine";

export class SubredditServices {
    static init(engine: DesktopEngine){
        this.threads = 
            RedditElements.generateListOf
	    <DesktopRedditThreadFromElement>("link",
                DesktopRedditThreadFromElement);
        RedditElements.hookSearch();
	if(DesktopEngine.isLoggedIn) {
	    RedditElements.hookAllSubscribeButtons(engine.subredditName!);
	}
    }
    static threads: Dictionary<RedditThread>;
}
