export interface HasElement {
    element: HTMLElement;
}