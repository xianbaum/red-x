import { Votable } from "./Votable";
import { Usertext } from "./Usertext";
import { Thingable } from "./Thingable";
import { LinkDomain } from "../LinkDomain";
import { Expando } from "./Expando";

export interface RedditThread extends Thingable, Usertext, Expando {
    //TODO THis
    vote(dir: -1 | 0 | 1): void;
    element: HTMLDivElement;
    fullname: string;
    domain: LinkDomain;
    link: string | undefined;
}
