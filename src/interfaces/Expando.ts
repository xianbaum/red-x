import { LinkDomain } from "../LinkDomain";

export interface Expando {
    getExpando(): Promise<string | HTMLElement>
    domain: LinkDomain;
}