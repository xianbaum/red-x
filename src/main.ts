import { RedditMaster } from "./RedditMaster";
import { AccountApi } from "./RedditApi";
import { DesktopEngine } from "./desktopengine/DesktopEngine";

if(!(<any>window).redditInitialized) {
    console.log("red-x is initialized.");
    RedditMaster.verifyCode().then( () => {
        (<any>window).redditInitialized = true;
	try {
            var x = new DesktopEngine();
	} catch (error) {
	    console.error(error);
	}
    });
}
